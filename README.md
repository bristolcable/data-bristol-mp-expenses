# Expenses: £25k of first class travel claimed by former MP & Bristol West candidate Stephen Williams

<img src="https://gitlab.com/bristolcable/data-bristol-mp-expenses/raw/master/first-or-not2.png" width="300">

On 2nd June 2017 we published an investigation into Bristol MP expenses, where we found that former Bristol West MP Stephen Williams claimed substantial expenses for first class travel.

# Get the Data

* IPSA: [The raw data is derrived from the Individual Claims export from the IPSA website.](http://www.theipsa.org.uk/mp-costs/other-published-data/)
* Subset: [IPSA data filtered and analysed](https://gitlab.com/bristolcable/data-bristol-mp-expenses/blob/master/travel_data.csv)
* Aggregated: [Bristol MP Travel Fares aggregated (both claimed and paid)](https://gitlab.com/bristolcable/data-bristol-mp-expenses/blob/master/3.1%20mp_travel_exp_all_recs%20(claimed+paid).xlsx)
* Fare History: [Bristol Temple Meads to London Paddington, historic Fares](https://gitlab.com/bristolcable/data-bristol-mp-expenses/blob/master/BRI%20to%20PAD%20historic%20train%20fares.xlsx) - Data from [farehistory.info](http://www.farehistory.info/)

# Visualisation and interactivity

* Bar chart: [First or Not](https://gitlab.com/bristolcable/data-bristol-mp-expenses/blob/master/first-or-not2.png)
* Bar chart: [MP Travel Type](https://gitlab.com/bristolcable/data-bristol-mp-expenses/blob/master/expense_claims_chart.png)
* Tableau Data Viz: [MP Travel Type](https://public.tableau.com/profile/publish/mp_expenses/Dashboard1)